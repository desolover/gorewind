### gorewind
Библиотека для работы с астрономическими и географическими объектами в сферической системе координат.

#### Список поддерживаемых каталогов
* [Yale Catalogue of Bright Stars](http://cdsarc.u-strasbg.fr/viz-bin/Cat?V/50) (BSC)
* [New General Catalogue of Nebulae and Clusters of Stars](https://cdsarc.unistra.fr/viz-bin/cat/VII/118) (NGC)
* [gitlab.com/desolover/astrocat](https://gitlab.com/desolover/astrocat)
* [GeoNames Data](http://download.geonames.org/export/dump/)

#### Теория
* [Эклиптическая система координат](https://ru.wikipedia.org/wiki/%D0%AD%D0%BA%D0%BB%D0%B8%D0%BF%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D0%BA%D0%BE%D0%BE%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D1%82) / Википедия
* [Сферическая система координат](https://ru.wikipedia.org/wiki/%D0%A1%D1%84%D0%B5%D1%80%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D0%BA%D0%BE%D0%BE%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D1%82) / Википедия